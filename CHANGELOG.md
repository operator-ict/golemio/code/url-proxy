# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.14] - 2025-02-13

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [1.2.13] - 2024-12-04

### Changed

-   Update Node.js to v20.18.0 ([core#119](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/119))

## [1.2.12] - 2024-10-29

### Security

-   dependency `http-proxy-middleware` updated from v2.0.6 to v2.0.7 ([CVE-2024-21536](https://security.snyk.io/vuln/SNYK-JS-HTTPPROXYMIDDLEWARE-8229906))

## [1.2.11] - 2024-09-24

-   No changelog

## [1.2.10] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.9] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.7] - 2023-09-13

### Fixed

-   memory leak caused by creating new instancies of ProxyMiddleware. ([url-proxy#3](https://gitlab.com/operator-ict/golemio/code/url-proxy/-/issues/3))

## [1.2.6] - 2023-09-04

### Added

-   Remote debugging using process signals

## [1.2.5] - 2023-07-31

### Changed

-   Update Node.js to v18.17.0

## [1.2.4] - 2023-07-17

### Fixed

-   Open telemetry initialization

## [1.2.3] - 2023-06-21

### Fixed

-   Preset path rewrite regex

## [1.2.1] - 2023-03-15

-   No changelog

## [1.2.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [1.0.6] - 2022-11-29

## Changed

-   logging changed to pino

## [1.0.5] - 2022-10-13

## Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))
-   Update TypeScript to v4.7.2

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.4] - 2022-08-05

### Changed

-   minor devops changes for Azure migrations, audit fix

## [1.0.3] - 2022-02-16

### Remove

-   Stop serving x-powered-by header

## [1.0.1] - 2021-11-23

### Changed

-   Utilize pid schema, modularized migrations ([pid#53](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/53))
-   Upgrade apicache

