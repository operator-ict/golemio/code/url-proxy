INSERT INTO ropid_departures_presets (route_name, api_version, route, url_query_params, note, created_at, updated_at)
    VALUES (
        'apex-zahradnimesto-6048', 2, '/pid/departureboards',
        'minutesAfter=60&limit=6&mode=departures&filter=routeHeadingOnce&order=real&aswIds=183_2',
        'Tablo Zahradní město (nyní pro testovací účely Chodovská B)', NOW(), NOW()
    );
