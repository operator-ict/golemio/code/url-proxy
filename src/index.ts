// load telemetry before all deps
import { initTraceProvider } from "@golemio/core/dist/monitoring";
initTraceProvider();

// start app
import { InspectorUtils } from "@golemio/core/dist/helpers/inspector";
import { config } from "./core/config";
import App from "./App";
new App().start();

const inspectorUtils = new InspectorUtils();

// handle user-defined process signals
process.on("SIGUSR1", () => inspectorUtils.activateInspector(config.inspector.host));
process.on("SIGUSR2", () => inspectorUtils.deactivateInspector());
