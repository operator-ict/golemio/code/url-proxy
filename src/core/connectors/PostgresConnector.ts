import { CustomError } from "@golemio/errors";
import { Sequelize } from "sequelize";
import { config } from "../config";
import { log } from "../helpers";

export class MySequelize {
    private connection?: Sequelize;

    public connect = async (connectionString = config.POSTGRES_CONN): Promise<Sequelize> => {
        if (this.connection) {
            return this.connection;
        }

        if (!connectionString) {
            throw new CustomError("The ENV variable POSTGRES_CONN cannot be undefined.", true, this.constructor.name);
        }

        try {
            this.connection = new Sequelize(connectionString, {
                define: {
                    freezeTableName: true,
                    timestamps: true, // adds createdAt and updatedAt
                    createdAt: "created_at",
                    updatedAt: "updated_at",
                    underscored: true, // automatically set field option for all attributes to snake cased name
                },
                logging: log.silly, // logging by Logger::
                pool: {
                    acquire: 60000,
                    idle: 10000,
                    max: 10,
                    min: 0,
                },
                retry: {
                    match: [
                        /SequelizeConnectionError/,
                        /SequelizeConnectionRefusedError/,
                        /SequelizeHostNotFoundError/,
                        /SequelizeHostNotReachableError/,
                        /SequelizeInvalidConnectionError/,
                        /SequelizeConnectionTimedOutError/,
                        /TimeoutError/,
                    ],
                    max: 8,
                },
            });

            if (!this.connection) {
                throw new Error("Connection is undefined.");
            }

            await this.connection.authenticate();
            log.info("Connected to PostgreSQL!");
            return this.connection;
        } catch (err) {
            throw new CustomError("Error while connecting to PostgreSQL.", false, this.constructor.name, undefined, err);
        }
    };

    public getConnection = (): Sequelize => {
        if (!this.connection) {
            throw new CustomError(
                "Sequelize connection does not exist. First call connect() method.",
                false,
                this.constructor.name,
                1002
            );
        }
        return this.connection;
    };

    public isConnected = async (): Promise<boolean> => {
        try {
            await this.connection!.authenticate();
            return true;
        } catch (err) {
            return false;
        }
    };

    public disconnect = async (): Promise<void> => {
        log.info("PostgreSQL disconnect called");
        if (this.connection) {
            await this.connection.close();
        }
    };
}

const PostgresConnector = new MySequelize();

export { PostgresConnector };
