import { createLogger, createRequestLogger } from "@golemio/core/dist/helpers";
import { config } from "../config";

const logger = createLogger({
    projectName: "url-proxy",
    nodeEnv: config.NODE_ENV,
    logLevel: config.LOG_LEVEL,
});

const requestLogger = createRequestLogger(config.NODE_ENV, logger);

export { logger as log, requestLogger };
