import { Request } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IDefinitionSource, IRouteDefinition, RopidDeparturesPresets } from "../../definitions";
import { config } from "../config";
import { RopidUrlParser } from "./helpers/RopidUrlParser";

export class ProxyRouter {
    private definitionSources: IDefinitionSource[] = [];
    private definitions: Record<string, IRouteDefinition> = {};
    private static apiVersions: { [v: number]: { url: string; apikey?: string } } = {
        2: {
            apikey: config.targets.golemioApiV2Apikey,
            url: config.targets.golemioApiV2Url,
        },
    };

    constructor() {
        this.definitionSources = [new RopidDeparturesPresets()];
    }

    public customRouter = (req: Request, definitions = this.definitions) => {
        const routeName = RopidUrlParser.getRouteName(req.originalUrl);
        const routeDefinition = definitions[routeName];

        if (!routeDefinition) {
            throw new GeneralError("Not found", ProxyRouter.name, `Unable to find route for ${req.originalUrl}`, 404);
        }

        return ProxyRouter.getVersionPath(routeDefinition?.apiVersion, routeDefinition.routeName) + routeDefinition?.url;
    };

    public customRewriter(path: string, req: Request) {
        return "";
    }

    public getHeaders(): { [key: string]: string } {
        return {
            "x-access-token": ProxyRouter.apiVersions[2].apikey || "",
        };
    }

    public async refreshRoutes(): Promise<void> {
        let newDefinitions: Record<string, IRouteDefinition> = {};
        for (const source of this.definitionSources) {
            const sourceDefinitions = await source.getRouteDefinitions();

            for (const sourceDefinition of sourceDefinitions) {
                newDefinitions[sourceDefinition.routeName] = sourceDefinition;
            }
        }
        this.definitions = newDefinitions;
    }

    private static getVersionPath(version: number | undefined, routeName: string) {
        if (!version) {
            throw new GeneralError("Not found", ProxyRouter.name, `Unknown version for route ${routeName}`, 404);
        }

        const apiInfo = ProxyRouter.apiVersions[version];
        if (!apiInfo) {
            throw new GeneralError(
                "Not found",
                ProxyRouter.name,
                `Undefined api for route ${routeName} and version ${version}`,
                404
            );
        }

        return apiInfo.url;
    }
}
