import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class RopidUrlParser {
    private static routeNameWithPrefixRegex = /\/pid\/(?<routeName>[^\/\?]+)(\/[^\/\?]*)?\??[^\/\?]*$/;

    public static getRouteName(url: string): string {
        const match = url.match(RopidUrlParser.routeNameWithPrefixRegex);
        if (!match || !match.groups) {
            throw new GeneralError("Not found", RopidUrlParser.name, undefined, 404);
        }

        return match.groups.routeName;
    }
}
