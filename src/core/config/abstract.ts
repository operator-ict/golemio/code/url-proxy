export interface IConfiguration {
    LOG_LEVEL: string | undefined;
    NODE_ENV: string;
    POSTGRES_CONN: string | undefined;
    app_version: string | undefined;
    cron_enabled: boolean;
    cron_time: string;
    port: string | undefined;
    inspector: {
        host: string | undefined;
    };
    targets: {
        golemioApiV2Url: string;
        golemioApiV2Apikey: string;
    };
}
