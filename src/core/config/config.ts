import { isValidCron } from "cron-validator";
import { IConfiguration } from "./abstract";

export const config: IConfiguration = {
    LOG_LEVEL: process.env.LOG_LEVEL,
    NODE_ENV: process.env.NODE_ENV || "development",
    POSTGRES_CONN: process.env.POSTGRES_CONN,
    app_version: process.env.npm_package_version,
    cron_enabled: process.env.CRON_ENABLED === "true",
    cron_time:
        process.env.CRON_TIME && isValidCron(process.env.CRON_TIME, { seconds: true }) ? process.env.CRON_TIME : "40 * * * * *",
    port: process.env.PORT,
    inspector: {
        host: process.env.INSPECTOR_HOST,
    },
    targets: {
        golemioApiV2Url: process.env.GOLEMIO_API_V2_URL || "localhost:3004",
        golemioApiV2Apikey: process.env.GOLEMIO_API_V2_APIKEY || "",
    },
};
