import { IServiceCheck, Service, getServiceHealth } from "@golemio/core/dist/helpers";
import express from "@golemio/core/dist/shared/express";
import { CustomError, ErrorHandler, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import { Lightship, createLightship } from "@golemio/core/dist/shared/lightship";
import fs from "fs";
import http from "http";
import { createProxyMiddleware } from "http-proxy-middleware";
import path from "path";
import { config } from "./core/config";
import { PostgresConnector } from "./core/connectors";
import { cronTasksManager, log, requestLogger } from "./core/helpers";
import { ProxyRouter } from "./core/router/ProxyRouter";

export default class App {
    /// Create a new express application instance
    public express: express.Application = express();
    public server?: http.Server;
    /// The port the express app will listen on
    public port: number = parseInt(config.port || "3010", 10);
    /// The SHA of the last commit from the application running
    private commitSHA: string | undefined = undefined;
    private lightship: Lightship;
    private router: express.Router;
    private proxyRouter?: ProxyRouter;

    /**
     * Runs configuration methods on the Express instance
     * and start other necessary services (crons, database, middlewares).
     */
    constructor() {
        this.lightship = createLightship({ shutdownHandlerTimeout: 10000 });
        process.on("uncaughtException", (err: Error) => {
            log.warn("url-proxy uncaughtException");
            log.error(err);
            this.lightship.shutdown();
        });
        process.on("unhandledRejection", (err: Error) => {
            log.warn("url-proxy unhandledRejection");
            log.error(err);
            this.lightship.shutdown();
        });
        this.router = express.Router();
    }

    /**
     * Starts the application
     */
    public start = async (): Promise<void> => {
        try {
            this.commitSHA = await this.loadCommitSHA();
            log.info(`Commit SHA: ${this.commitSHA}`);
            await this.database();

            this.proxyRouter = new ProxyRouter();
            await this.proxyRouter.refreshRoutes();

            await this.expressServer();

            this.registerCronTasks();
            if (config.cron_enabled) {
                cronTasksManager.startAll();
            }
            cronTasksManager.checkStatus();

            log.info("Started!");
            this.lightship.registerShutdownHandler(async () => {
                await this.gracefulShutdown();
            });
            this.lightship.signalReady();
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }
    };

    /**
     * Starts the database connection with initial configuration
     */
    private database = async (): Promise<void> => {
        await PostgresConnector.connect();
    };

    /**
     * Graceful shutdown - terminate connections and server
     */
    private gracefulShutdown = async (): Promise<void> => {
        log.info("Graceful shutdown initiated.");
        await PostgresConnector.disconnect();
        await this.server?.close();
    };

    /**
     * Loading the Commit SHA of the current build
     */
    private loadCommitSHA = async (): Promise<string> => {
        return new Promise<string>((resolve) => {
            fs.readFile(path.join(__dirname, "..", "commitsha"), (err, data) => {
                if (err) {
                    return resolve(undefined as any);
                }
                return resolve(data.toString());
            });
        });
    };

    /**
     * Starts the express server
     */
    private async expressServer(): Promise<void> {
        this.middleware();
        await this.routes();

        this.server = http.createServer(this.express);
        // Setup error handler hook on server error
        this.server.on("error", (err: any) => {
            ErrorHandler.handle(new CustomError("Could not start a server", false, undefined, 1, err), log, "error");
        });
        // Serve the application at the given port
        this.server.listen(this.port, () => {
            // Success callback
            log.info(`Listening at http://localhost:${this.port}/`);
        });
    }

    /**
     * Checking app health
     */
    private healthCheck = async () => {
        const description = {
            app: "Golemio Data Platform URL Proxy",
            commitSha: this.commitSHA,
            version: config.app_version,
        };

        const services: IServiceCheck[] = [{ name: Service.POSTGRES, check: PostgresConnector.isConnected }];

        const serviceStats = await getServiceHealth(services);

        return { ...description, ...serviceStats };
    };

    /**
     * Registers cron tasks
     */
    private registerCronTasks = () => {
        cronTasksManager.registerTask({
            cronTime: config.cron_time,
            name: "Reload router definitions",
            process: async () => {
                await this.proxyRouter!.refreshRoutes();
            },
        });
    };

    /**
     * Using express middlewares
     */
    private middleware = (): void => {
        this.express.disable("x-powered-by");
        this.express.use(requestLogger);
    };

    /**
     * Defines express server routes
     */
    private routes = async (): Promise<void> => {
        const defaultRouter = express.Router();

        // base url route handler
        defaultRouter.get(
            ["/", "/healthcheck", "/status"],
            async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                try {
                    const healthStats = await this.healthCheck();
                    if (healthStats.health) {
                        return res.json(healthStats);
                    } else {
                        return res.status(503).send(healthStats);
                    }
                } catch (err) {
                    return res.status(503);
                }
            }
        );

        this.express.use("/", defaultRouter);
        this.express.use((req, res, next) => {
            if (this.router === null) {
                return next();
            }
            this.router(req, res, next);
        });
        this.router = await this.prepareRouter();

        // Not found error - no route was matched
        this.express.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            next(new CustomError("Not found", true, undefined, 404));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            const warnCodes = [400, 401, 403, 404, 409, 413, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };

    public async prepareRouter(): Promise<express.Router> {
        const router = express.Router();
        const middleware = createProxyMiddleware({
            headers: this.proxyRouter!.getHeaders(),
            router: this.proxyRouter!.customRouter,
            changeOrigin: true,
            pathRewrite: this.proxyRouter!.customRewriter,
            logProvider: (_provider) => ({
                debug: log.debug.bind(log),
                log: log.info.bind(log),
                info: log.info.bind(log),
                warn: log.warn.bind(log),
                error: log.error.bind(log),
            }),
            xfwd: true,
        });
        router.use(`/`, middleware);

        return router;
    }
}
