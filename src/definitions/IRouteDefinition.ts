export interface IRouteDefinition {
    apiVersion: number;
    routeName: string;
    url: string;
}
