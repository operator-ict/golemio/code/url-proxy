import { IRouteDefinition } from "./";

export interface IDefinitionSource {
    getRouteDefinitions(): Promise<IRouteDefinition[]>;
}
