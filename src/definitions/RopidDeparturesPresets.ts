import { PG_SCHEMA } from "@golemio/pid/dist/schema-definitions/const";
import {
    IRopidDeparturesPresetsOutput,
    RopidDeparturesPresets as schema,
} from "@golemio/pid/dist/schema-definitions/ropid-departures-presets";
import { ModelCtor } from "sequelize";

import { PostgresConnector } from "../core/connectors";
import { IDefinitionSource, IRouteDefinition } from "./";

export class RopidDeparturesPresets implements IDefinitionSource {
    private model: ModelCtor<any>;

    constructor() {
        this.model = PostgresConnector.getConnection().define(schema.pgTableName, schema.outputSequelizeAttributes, {
            // Remove all audit fields in the default scope: they will be automatically excluded,
            // as they're not needed in the output view, but are present in the table/data
            schema: PG_SCHEMA,
            defaultScope: {
                attributes: {
                    exclude: ["created_by", "updated_by", "created_at", "updated_at", "create_batch_id", "update_batch_id"],
                },
            },
        });
    }

    public async getRouteDefinitions(): Promise<IRouteDefinition[]> {
        const presets = await this.getAll();

        return presets.map((preset) => {
            return {
                apiVersion: preset.api_version,
                routeName: preset.route_name,
                url: `${preset.route}?${preset.url_query_params}`,
            };
        });
    }

    private async getAll(): Promise<IRopidDeparturesPresetsOutput[]> {
        return (await this.model.findAll({ raw: true })) as IRopidDeparturesPresetsOutput[];
    }
}
