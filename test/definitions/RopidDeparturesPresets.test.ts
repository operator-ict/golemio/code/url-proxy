import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "../../src/core/connectors";
import { RopidDeparturesPresets } from "../../src/definitions";

chai.use(chaiAsPromised);

describe("RopidDeparturesPresets", () => {
    let definition: RopidDeparturesPresets;

    before(async () => {
        await PostgresConnector.connect();
    });

    beforeEach(async () => {
        definition = new RopidDeparturesPresets();
    });

    it("should gets all route definitions", async () => {
        const data = await definition.getRouteDefinitions();
        expect(data).to.deep.equal([
            {
                apiVersion: 2,
                routeName: "apex-zahradnimesto-6048",
                url:
                    `/pid/departureboards?` +
                    `minutesAfter=60&limit=6&mode=departures&filter=routeHeadingOnce&order=real&aswIds=183_2`,
            },
        ]);
    });
});
