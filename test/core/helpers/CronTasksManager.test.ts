import { expect } from "chai";
import "mocha";
import * as sinon from "sinon";
import { cronTasksManager, log } from "../../../src/core/helpers";

describe("CronTasksManager", () => {
    let sandbox: sinon.SinonSandbox;
    let testFunc: sinon.SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        testFunc = sandbox.stub();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has registerTask method", async () => {
        expect(cronTasksManager.registerTask).not.to.be.undefined;
    });

    it("should has checkStatus method", async () => {
        expect(cronTasksManager.checkStatus).not.to.be.undefined;
    });

    it("should has startAll method", async () => {
        expect(cronTasksManager.startAll).not.to.be.undefined;
    });

    it("should has stopAll method", async () => {
        expect(cronTasksManager.stopAll).not.to.be.undefined;
    });

    it("should register new task", () => {
        cronTasksManager.registerTask({
            cronTime: "*/2 * * * * *",
            name: "test",
            process: () => {
                log.debug("I'm running.");
                testFunc();
                return Promise.resolve();
            },
        });
        expect(Object.keys(cronTasksManager.getRunningTasks())).to.be.an("array").that.is.empty;
        expect(Object.keys(cronTasksManager.getStoppedTasks())).to.be.an("array").that.is.not.empty;
        cronTasksManager.checkStatus();
    });

    it("should start all", async () => {
        cronTasksManager.startAll();
        expect(Object.keys(cronTasksManager.getRunningTasks())).to.be.an("array").that.is.not.empty;
        expect(Object.keys(cronTasksManager.getStoppedTasks())).to.be.an("array").that.is.empty;
        cronTasksManager.checkStatus();
        await new Promise((resolve) => setTimeout(resolve, 2000));
        sandbox.assert.calledOnce(testFunc);
    });

    it("should stop all", () => {
        cronTasksManager.stopAll();
        expect(Object.keys(cronTasksManager.getRunningTasks())).to.be.an("array").that.is.empty;
        expect(Object.keys(cronTasksManager.getStoppedTasks())).to.be.an("array").that.is.not.empty;
        cronTasksManager.checkStatus();
    });

    it("should handle throwable", async () => {
        cronTasksManager.registerTask({
            cronTime: "*/2 * * * * *",
            name: "test",
            process: () => {
                log.debug("I'm running.");
                throw new Error("I'm failed.");
            },
        });
        cronTasksManager.startAll();
        cronTasksManager.checkStatus();
        await new Promise((resolve) => setTimeout(resolve, 2000));
        cronTasksManager.stopAll();
    });
});
