import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { RopidUrlParser } from "../../../../src/core/router/helpers/RopidUrlParser";

chai.use(chaiAsPromised);

describe("RopidUrlParser", () => {
    it("should parse routeNames properly", async () => {
        expect(RopidUrlParser.getRouteName("/pid/mobatime-cerny_most-17-5980")).to.deep.equal("mobatime-cerny_most-17-5980");
        expect(RopidUrlParser.getRouteName("/pid/herman-stredokluky_nad_beloky-b-8230")).to.deep.equal(
            "herman-stredokluky_nad_beloky-b-8230"
        );
        expect(RopidUrlParser.getRouteName("/pid/herman-stredokluky_nad_beloky-b-8230?param=0")).to.deep.equal(
            "herman-stredokluky_nad_beloky-b-8230"
        );
        expect(RopidUrlParser.getRouteName("/pid/herman-stredokluky_nad_beloky-b-8230/test")).to.deep.equal(
            "herman-stredokluky_nad_beloky-b-8230"
        );
        expect(RopidUrlParser.getRouteName("/pid/herman-stredokluky_nad_beloky-b-8230/test?param=0")).to.deep.equal(
            "herman-stredokluky_nad_beloky-b-8230"
        );
        expect(RopidUrlParser.getRouteName("/pid/herman-stredokluky_nad_beloky-b-8230/")).to.deep.equal(
            "herman-stredokluky_nad_beloky-b-8230"
        );
        expect(() => RopidUrlParser.getRouteName("/pidx/herman-stredokluky_nad_beloky-b-8230?param=0")).to.throw(GeneralError);
    });
});
