import { CustomError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import { PostgresConnector } from "../../../src/core/connectors";

chai.use(chaiAsPromised);

describe("PostgresConnector", () => {
    it("should throws Error if not connect method was not called", async () => {
        expect(() => {
            PostgresConnector.getConnection();
        }).to.throw(CustomError);
    });

    it("should connects to PostgreSQL and returns connection", async () => {
        const ch = await PostgresConnector.connect();
        expect(ch).to.be.an.instanceof(Object);
        expect(PostgresConnector.getConnection()).to.be.an.instanceof(Object);
    });

    it("should checks the connector is connected", async () => {
        expect(await PostgresConnector.isConnected()).to.be.true;
    });
});
