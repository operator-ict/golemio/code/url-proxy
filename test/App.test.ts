import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import "mocha";
import request, { Response } from "supertest";
import sinon from "sinon";
import express from "@golemio/core/dist/shared/express";
import { config } from "../src/core/config";
import App from "../src/App";

chai.use(chaiAsPromised);

describe("App", () => {
    let expressApp: express.Application;
    let app: App;
    let sandbox: any;
    let exitStub: any;

    before(async () => {
        app = new App();
        await app.start();
        expressApp = app.express;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        exitStub = sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should start", async () => {
        expect(expressApp).not.to.be.undefined;
    });

    it("should have all config variables set", () => {
        expect(config).not.to.be.undefined;
        expect(config.POSTGRES_CONN).not.to.be.undefined;
    });

    it("should have health check on /", (done) => {
        request(expressApp).get("/").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should have health check on /health-check", (done) => {
        request(expressApp)
            .get("/healthcheck")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect((res: Response) => {
                expect(res.body).to.deep.include({
                    app: "Golemio Data Platform URL Proxy",
                    health: true,
                    services: [
                        {
                            name: "PostgreSQL",
                            status: "UP",
                        },
                    ],
                });
            })
            .expect(200, done);
    });

    it("should return 404 on non-existing route /non-existing", (done) => {
        request(expressApp)
            .get("/non-existing")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404, done);
    });
});
